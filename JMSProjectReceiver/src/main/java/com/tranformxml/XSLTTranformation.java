package com.tranformxml;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.send.jms.QueueSenderAfterHazelCast;

public class XSLTTranformation {

	public static void trasformXML(String inXML) {
		String inXSL = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">"
				+ "<xsl:output method=\"xml\" indent=\"yes\" />"
				+ "<xsl:template match=\"Employee\">"
				+ "<xsl:copy><xsl:copy-of select=\"Employee\" />"
				+ "<Name><xsl:value-of select=\"concat(FirstName, '_', LastName)\" /></Name>"
				+ "<Id><xsl:value-of select=\"Id\" /></Id>"
				+ "</xsl:copy></xsl:template></xsl:stylesheet>";
		
		System.out.println(inXSL);
		String outXML = "";
		XSLTTranformation st = new XSLTTranformation();
		try {
			st.transform(inXML, inXSL, outXML);
		} catch (TransformerConfigurationException e) {
			System.err.println("Invalid factory configuration");
			e.printStackTrace();
		} catch (TransformerException e) {
			System.err.println("Error during transformation");
			e.printStackTrace();
		}
	}

	public void transform(String inXML, String inXSL, String outXML)
			throws TransformerConfigurationException, TransformerException {
		TransformerFactory factory = TransformerFactory.newInstance();
		StreamSource xslStream = new StreamSource(new StringReader(inXSL));
		Transformer transformer = factory.newTransformer(xslStream);
		transformer.setErrorListener(new MyErrorListener());
		StreamSource in = new StreamSource(new StringReader(inXML));
		StringWriter xmlOutWriter = new StringWriter();
		transformer.transform(in,new StreamResult(xmlOutWriter));
		
		//get Eid from output xml content
		int length = xmlOutWriter.toString().length();
	    String eid = "";
	    for (int i = 0; i < length; i++) {
	        Character character = xmlOutWriter.toString().charAt(i);
	        if (Character.isDigit(character)) {
	            eid += character;
	        }
	    }
	    eid = eid.substring(3, 6);
	    System.out.println("******************  eid is: " + eid);
		
		try {
			QueueSenderAfterHazelCast.afterXSLT(eid,xmlOutWriter.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}